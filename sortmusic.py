import sys


def order_items(songs: list, i: int, gap: int) -> list:
    n = len(songs)
    for j in range(i + gap, n):
        current_song = songs[j]
        previous_song = songs[j - gap]

        if current_song[1] >= previous_song[1]:
            break
        else:
            songs[j], songs[j - gap] = songs[j - gap], songs[j]
    return songs


def sort_music(songs: list) -> list:
    gap = len(songs) // 2
    while gap > 0:
        for i in range(gap, len(songs)):
            temp = songs[i]
            j = i
            while j >= gap and songs[j - gap][1] < temp[1]:
                songs[j] = songs[j - gap]
                j -= gap
            songs[j] = temp
        gap //= 2
    return songs


def create_dictionary(arguments):
    song_dict = {}
    for i in range(0, len(arguments), 2):
        title = arguments[i]
        plays = int(arguments[i + 1])
        song_dict[title] = plays
    return song_dict


def main():
    args = sys.argv[1:]

    if len(args) % 2 != 0 or len(args) == 0:
        sys.exit("Error: Debes proporcionar pares de canción y número de reproducciones.")

    songs: dict = create_dictionary(args)

    sorted_songs: list = sort_music(list(songs.items()))

    sorted_dict: dict = dict(sorted_songs)
    print(sorted_dict)


if __name__ == '__main__':
    main()
